import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.ConditionType as ConditionType
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable

WebUI.openBrowser('')

WebUI.maximizeWindow()

WebUI.navigateToUrl(GlobalVariable.URL)

WebUI.click(findTestObject('Page_Home/Lnk_Login'))

WebUI.waitForPageLoad(3)

WebUI.setText(findTestObject('Page_Login/Txt_Email'), 'prafulla.hari11@gmail.com')

WebUI.setText(findTestObject('Page_Login/Txt_password'), 'sparity@123')

WebUI.click(findTestObject('Page_Login/Btn_Login'))

WebUI.waitForPageLoad(3)

WebUI.click(findTestObject('Page_Home/Lnk_MEN'))

WebUI.waitForPageLoad(10)

WebUI.click(findTestObject('Page_MEN/Lnk_Size'))

WebUI.click(findTestObject('Page_MEN/Lnk_VariantXS'))

WebUI.waitForPageLoad(10)

WebUI.click(findTestObject('Page_MEN/Lnk_Product'))

WebUI.waitForPageLoad(10)

WebUI.verifyElementPresent(findTestObject('Page_Detail/Btn_Checkout'), 0)

WebUI.verifyTextNotPresent('Hide Description', false)

WebUI.verifyElementText(findTestObject('Page_Detail/Btn_AddToCart'), 'ADD TO CART')

Lbl_AddToCart = WebUI.getText(findTestObject('Page_Detail/Btn_AddToCart'))

WebUI.verifyMatch(Lbl_AddToCart, findTestData('ButtonText/Btn_Text').getValue(1, 1), false)

Lbl_Checkout = WebUI.getText(findTestObject('Page_Detail/Btn_Checkout'))

WebUI.verifyMatch(Lbl_Checkout, findTestData('ButtonText/Btn_Text').getValue(1, 2), false)

WebUI.click(findTestObject('Page_Detail/Btn_Checkout'))

WebUI.waitForPageLoad(3)

String XpathContinue = '(//button[@title="Continue"])/child::span'

TestObject Btn_Continue = new TestObject(XpathContinue)

Btn_Continue.addProperty('xpath', ConditionType.EQUALS, XpathContinue, true)

Lbl_Continue = WebUI.getText(Btn_Continue)

WebUI.verifyMatch(Lbl_Continue, findTestData('ButtonText/Btn_Text').getValue(1, 3), false)

WebUI.click(Btn_Continue)


